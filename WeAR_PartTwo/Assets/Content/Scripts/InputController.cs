﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController: MonoBehaviour 
{
	void Start () 
  {
    StartCoroutine("CheckInputCoroutine");
	}
	
  private IEnumerator CheckInputCoroutine()
  {
    while(true)
    {
      MobileInput();
      yield return null;
    }
  }

  private void MobileInput()
  {
    if(Input.touchCount > 0)
    {
      if(Input.GetTouch(0).phase == TouchPhase.Began)
      {
        CheckRaycast(Input.GetTouch(0).position);
      }
    }
  }

  private void CheckRaycast(Vector3 reycastPosition)
  {
    Ray cursorRay = Camera.main.ScreenPointToRay(reycastPosition);
    RaycastHit hit;

    if(Physics.Raycast(cursorRay, out hit, 1000))
    {
      ObjectLinck objectLinck = hit.collider.GetComponent<ObjectLinck>();

      if(objectLinck != null)
      {
        Application.OpenURL(objectLinck.Url);
      }
    }   
  }
}