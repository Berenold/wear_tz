﻿/*============================================================================== 
 * Copyright (c) 2015 Qualcomm Connected Experiences, Inc. All Rights Reserved. 
 * ==============================================================================*/
using UnityEngine;


public class ModelSwap : MonoBehaviour 
{
  private GameObject mDefaultModel;
  private GameObject mActiveModel = null;
  private TrackableSettings mTrackableSettings = null;

	private void Start () 
  {
    mDefaultModel = this.transform.FindChild("teapot").gameObject;
    mActiveModel = mDefaultModel;
    mTrackableSettings = FindObjectOfType<TrackableSettings>();
  }
}
