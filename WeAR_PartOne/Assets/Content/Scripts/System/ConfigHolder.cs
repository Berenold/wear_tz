﻿using UnityEngine;

public class ConfigHolder: MonoBehaviour
{
  private float _playerPawnMoveSpeed;
  private float _playerPawnRotateSpeed;
  private float _playerInputMoveTime;
  private float _enemySpawnTime;

  public float PlayerPawnMoveSpeed
  {
    get { return _playerPawnMoveSpeed; }
  }
  public float PlayerPawnRotateSpeed
  {
    get { return _playerPawnRotateSpeed; }
  }
  public float PlayerInputMoveTime
  {
    get { return _playerInputMoveTime; }
  }
  public float EnemySpawnTime
  {
    get { return _enemySpawnTime; }
  }

  public void InitConfig(GameConfig gameConfig)
  {
    DontDestroyOnLoad(gameObject);
    _playerPawnMoveSpeed = gameConfig.playerPawnMoveSpeed;
    _playerPawnRotateSpeed = gameConfig.playerPawnRotateSpeed;
    _playerInputMoveTime = gameConfig.playerInputMoveTime;
    _enemySpawnTime = gameConfig.enemySpawnTime;
  }
}

[System.Serializable]
public class GameConfig
{
  public float playerPawnMoveSpeed;
  public float playerPawnRotateSpeed;
  public float playerInputMoveTime;
  public float enemySpawnTime;
}