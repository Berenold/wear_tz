﻿using UnityEngine;
using System;

public class EventHolder: MonoBehaviour 
{
  public Action pawnSelfDestroyEvent;
  public void PawnSelfDestroy()
  {
    if(pawnSelfDestroyEvent != null)
    {
      pawnSelfDestroyEvent();
    }
  }
}