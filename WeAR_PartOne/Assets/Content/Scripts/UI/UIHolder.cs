﻿using UnityEngine;

public class UIHolder: Singleton<UIHolder>
{
  [SerializeField]
  private IndicatorPanelController _indicatorPanelController;
  [SerializeField]
  private InfoPanelController _infoPanelController;

  public IndicatorPanelController IndicatorPanel
  {
    get { return _indicatorPanelController; }
    set { _indicatorPanelController = value; }
  }

  public InfoPanelController InfoPanel
  {
    get { return _infoPanelController; }
    set { _infoPanelController = value; }
  }

  public void ClearScene()
  {
    ApplicationContainer.Instance.EventHolder.PawnSelfDestroy();
  }
}