﻿using UnityEngine;
using UnityEngine.UI;

public class IndicatorPanelController: MonoBehaviour 
{
  [SerializeField]
  private Image _indicator;
  private float _indicatorTime;

  public void InitPanel(float indicatorTime, Vector3 touchPosition)
  {
    _indicator.transform.position = new Vector3(touchPosition.x, touchPosition.y, _indicator.transform.position.z);
    _indicatorTime = indicatorTime;
    _indicator.fillAmount = 0;
  }

  public void ChangeIndicatorValue(float touchTime)
  {
    if(touchTime > _indicatorTime)
    {
      touchTime = _indicatorTime;
    }

    _indicator.fillAmount = touchTime/_indicatorTime;
  }
}