﻿using UnityEngine;
using UnityEngine.UI;

public class InfoPanelController: MonoBehaviour 
{
  [SerializeField]
  private Text _score;
  [SerializeField]
  private Text _enemyCount;

  public void SetScore()
  {
    _score.text = ApplicationContainer.Instance.Score.ToString();
  }

  public void SetEnemyCount()
  {
    _enemyCount.text = ApplicationContainer.Instance.EnemyCount.ToString();
  }
}