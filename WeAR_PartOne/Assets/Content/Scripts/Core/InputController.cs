﻿using UnityEngine;
using System.Collections;

public class InputController: MonoBehaviour 
{
  private float _playerInputMoveTime;
  private float _timeShift;
  private bool _movePlayerPawn = false;

  public void CheckInput(bool checkInput , float playerInputMoveTime)
  {
    if(checkInput)
    {
      _playerInputMoveTime = playerInputMoveTime;
      StartCoroutine("CheckInputCoroutine");
    }
    else
    {
      StopCoroutine("CheckInputCoroutine");
    }
  }

  private IEnumerator CheckInputCoroutine()
  {
    while(true)
    {
      #if UNITY_ANDROID
      MobileInput();
      #endif

      #if UNITY_EDITOR
      PcInput();
      #endif

      yield return null;
    }
  }

  #if UNITY_ANDROID
  private void MobileInput()
  {
    if(Input.touchCount > 0)
    {
      switch(Input.GetTouch(0).phase)
      {
        case TouchPhase.Began:
          _timeShift = 0;
          UIHolder.Instance.IndicatorPanel.InitPanel(_playerInputMoveTime, Input.GetTouch(0).position);
          break;
        case TouchPhase.Ended:
          UIHolder.Instance.IndicatorPanel.ChangeIndicatorValue(0);
          Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.y);

          if(!_movePlayerPawn)
          {
            SetRotateTap(mousePosition);
          }
          else
          {
            _movePlayerPawn = false;
            SetMoveTap(mousePosition);
          }
          break;
        case TouchPhase.Stationary:
        case TouchPhase.Moved:
          if(!_movePlayerPawn)
          {
            _timeShift += Time.deltaTime;

            if(_timeShift > 0.2f)
            {
              UIHolder.Instance.IndicatorPanel.ChangeIndicatorValue(_timeShift);
            }

            if(_timeShift >= _playerInputMoveTime)
            {
              _movePlayerPawn = true;
            }
          }
          break;
      }
    }
  }
  #endif

  #if UNITY_EDITOR
  private void PcInput()
  {
    if(Input.GetMouseButtonDown(0))
    {
      _timeShift = 0;
      UIHolder.Instance.IndicatorPanel.InitPanel(_playerInputMoveTime, Input.mousePosition);
    }

    if(Input.GetMouseButton(0))
    {
      if(!_movePlayerPawn)
      {
        _timeShift += Time.deltaTime;

        if(_timeShift > 0.2f)
        {
          UIHolder.Instance.IndicatorPanel.ChangeIndicatorValue(_timeShift);
        }

        if(_timeShift >= _playerInputMoveTime)
        {
          _movePlayerPawn = true;
        }
      }
    }

    if(Input.GetMouseButtonUp(0))
    {
      UIHolder.Instance.IndicatorPanel.ChangeIndicatorValue(0);
      Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.y);

      if(!_movePlayerPawn)
      {
        SetRotateTap(mousePosition);
      }
      else
      {
        _movePlayerPawn = false;
        SetMoveTap(mousePosition);
      }
    }
  }
  #endif

  private void SetRotateTap(Vector3 worldInput)
  {
    ApplicationContainer.Instance.RotatePlayerPawn(Camera.main.ScreenToWorldPoint(worldInput));
  }

  private void SetMoveTap(Vector3 worldInput)
  {
    ApplicationContainer.Instance.MovePlayerPawn(Camera.main.ScreenToWorldPoint(worldInput));
  }
}