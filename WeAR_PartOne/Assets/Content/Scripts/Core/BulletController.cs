﻿using System.Collections;
using UnityEngine;

public class BulletController: MonoBehaviour 
{
  [SerializeField]
  private float _bulletSpeed;
  [SerializeField]
  private float _destroyDistance;
  private Transform _gun;

  public void DestroyBullet()
  {
    _gun.gameObject.GetComponent<PlayerPawnController>().AddBulletPullObject(gameObject);
    gameObject.transform.position = _gun.position;
    gameObject.SetActive(false);
  }

  public void InitBullet(Transform gun)
  {
    _gun = gun;
    StartCoroutine("MoveCoroutine");
  }

  private IEnumerator MoveCoroutine()
  {
    while(true)
    {
      transform.Translate(Vector3.forward * _bulletSpeed * Time.deltaTime);
     
      if(Vector3.Distance(_gun.position, transform.position) >= _destroyDistance)
      {
        DestroyBullet();
      }
        
      yield return null;
    }
  }
}
