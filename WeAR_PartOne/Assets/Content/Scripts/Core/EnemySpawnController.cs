﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnController: MonoBehaviour 
{
  [SerializeField]
  private GameObject _enemyPrefab;
  private float _spawnTime;
  private float _timeShift;
  private List<GameObject> _pullObjects = new List<GameObject>();
  public List<GameObject> PullObjects
  {
    get { return _pullObjects; }
    set { _pullObjects = value; }
  }

  public void AddPullObject(GameObject newPullObject)
  {
    _pullObjects.Add(newPullObject);
  }

  public void Spawn(bool spawnEmeny, float spawnTime)
  {
    if(spawnEmeny)
    {
      if(_enemyPrefab != null)
      {
        _spawnTime = spawnTime;
        _timeShift = _spawnTime;
        StartCoroutine("SpawnCoroutine");
      }
    }
    else
    {
      StopCoroutine("SpawnCoroutine");
    }
  }

  private IEnumerator SpawnCoroutine()
  {
    while(true)
    {
      _timeShift -=Time.fixedDeltaTime;

      if(_timeShift <= 0)
      {
        if(ApplicationContainer.Instance.EnemyCount < 10)
        {
          SelectPullObject();
        }

        _timeShift = _spawnTime;
      }

      yield return null;
    }
  }

  private void SelectPullObject()
  {
    if(_pullObjects.Count == 0)
    {
      if(_enemyPrefab != null)
      {
        AddNewPawn(Instantiate(_enemyPrefab));
      }
    }
    else
    {
      _pullObjects[0].SetActive(true);
      AddNewPawn(_pullObjects[0]);
      _pullObjects.RemoveAt(0);
    }
  }

  private void AddNewPawn(GameObject newEnemy)
  {
    newEnemy.transform.position = new Vector3(Random.Range(-7, 8), 0, Random.Range(-7, 8));
    ApplicationContainer.Instance.EnemyCount++;
    UIHolder.Instance.InfoPanel.SetEnemyCount();
  }
}