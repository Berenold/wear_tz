﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerPawnController: MonoBehaviour 
{
  [SerializeField]
  private GameObject _movePointParticle;
  [SerializeField]
  private GameObject _bulletParticle;
  [SerializeField]
  private GameObject _bulletPointParticle;
  private List<GameObject> _bulletPullObjects;
  private Transform _movePoint;
  private float _moveSpeed;
  private float _rotateSpeed;
  private bool _rotateCoroutineWorking;
  private bool _moveCoroutineWorking;

  public void AddBulletPullObject(GameObject newPullObject)
  {
    _bulletPullObjects.Add(newPullObject);
  }

  public void InitContoller(float moveSpeed, float rotateSpeed)
  {
    _moveSpeed = moveSpeed;
    _rotateSpeed = rotateSpeed;

    if(_movePointParticle != null)
    {
      _movePoint = Instantiate(_movePointParticle).transform;
      _movePoint.gameObject.SetActive(false);
    }
  }

  public void ChangeRoration(Vector3 shootTo)
  {
    if(_rotateCoroutineWorking)
    {
      StopCoroutine("ChangeRorationCoroutine");
    }

    if(_moveCoroutineWorking)
    {
      StopCoroutine("ChangePositionCoroutine");
      HideMovePointParticle();
    }

    StartCoroutine("ChangeRorationCoroutine", GetNewQuaternion(shootTo));
  }

  private IEnumerator ChangeRorationCoroutine(Quaternion newQuaternion)
  {
    _rotateCoroutineWorking = true;

    while(transform.rotation != newQuaternion)
    {
      transform.rotation = Quaternion.Lerp(transform.rotation, newQuaternion, Time.deltaTime * _rotateSpeed);

      if(Quaternion.Angle(transform.rotation, newQuaternion) <= 3)
      {
        transform.rotation = newQuaternion;
      }

      yield return null;
    }

    yield return true;
    Shoot();
    _rotateCoroutineWorking = false;
  }

  public void ChangePosition(Vector3 moveTo)
  {
    if(_moveCoroutineWorking)
    {
      StopCoroutine("ChangePositionCoroutine");
      HideMovePointParticle();
    }

    StartCoroutine("ChangePositionCoroutine", moveTo);
  }

  private IEnumerator ChangePositionCoroutine(Vector3 moveVector)
  {
    if(_movePoint != null)
    {
      _movePoint.position = new Vector3(moveVector.x, 0.5f, moveVector.z);
      _movePoint.gameObject.SetActive(true);
    }

    _moveCoroutineWorking = true;
    transform.rotation = GetNewQuaternion(moveVector);

    while(transform.position != moveVector)
    {
      transform.Translate(Vector3.forward * _moveSpeed * Time.deltaTime);
      if(Vector3.Distance(transform.position, moveVector) <= 0.2f)
      {
        transform.position = moveVector;
      }

      yield return null;
    }

    yield return true;
    HideMovePointParticle();
    _moveCoroutineWorking = false;
  }

  private Quaternion GetNewQuaternion(Vector3 clickPosition)
  {
    clickPosition -= transform.position;
    float angle = Mathf.Atan2(clickPosition.x, clickPosition.z) * Mathf.Rad2Deg;
    return Quaternion.AngleAxis(angle, Vector3.up);
  }

  private void HideMovePointParticle()
  { 
    if(_movePoint != null)
    {
      _movePoint.gameObject.SetActive(false);
    }
  }

  private void Shoot()
  {
    if(_bulletPullObjects.Count == 0)
    {
      if(_bulletParticle != null)
      {
        AddNewPawn(Instantiate(_bulletParticle));
      }
    }
    else
    {
      _bulletPullObjects[0].SetActive(true);
      AddNewPawn(_bulletPullObjects[0]);
      _bulletPullObjects.RemoveAt(0);
    }
  }

  private void AddNewPawn(GameObject newEnemy)
  {
    if(_bulletPointParticle != null)
    {
      newEnemy.transform.position = _bulletPointParticle.transform.position;
      newEnemy.transform.rotation = transform.rotation;
      newEnemy.GetComponent<BulletController>().InitBullet(transform);
    }
  }
}