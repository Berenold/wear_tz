﻿using UnityEngine;

public class EnemyPawnController: MonoBehaviour 
{
  private void OnEnable()
  {
    ApplicationContainer.Instance.EventHolder.pawnSelfDestroyEvent += PawnSelfDestroy;
  }

  private void PawnSelfDestroy()
  {
    DestroyPawn();
  }

  private void OnTriggerEnter(Collider boolet)
  {
    boolet.gameObject.GetComponent<BulletController>().DestroyBullet();
    ApplicationContainer.Instance.Score++;
    UIHolder.Instance.InfoPanel.SetScore();
    DestroyPawn();
  }

  private void DestroyPawn()
  {
    ApplicationContainer.Instance.EnemySpawnController.AddPullObject(gameObject);
    ApplicationContainer.Instance.EventHolder.pawnSelfDestroyEvent -= PawnSelfDestroy;
    ApplicationContainer.Instance.EnemyCount--;
    UIHolder.Instance.InfoPanel.SetEnemyCount();
    gameObject.SetActive(false);
  }
}