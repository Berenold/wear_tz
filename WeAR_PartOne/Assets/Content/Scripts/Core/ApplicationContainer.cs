﻿using UnityEngine;
using System;

public class ApplicationContainer: Singleton<ApplicationContainer>
{
  #region Variable part
  private const int FLOOR_WIDTH = 200;
  private const int FLOOR_HEIGHT = 200;

  [SerializeField]
  private bool _devMode;
  [SerializeField]
  private GameObject _playerPawn;
  [SerializeField]
  private GameObject _enemySpawnPrefab;
  [SerializeField]
  private Material _floorMaterial;
  private PlayerPawnController _playerPawnController;
  private InputController _inputController;
  private EnemySpawnController _enemySpawnController;
  private ConfigHolder _configHolder;
  private FileManager _fileManager;
  private EventHolder _eventHolder;
  private FpsView _fpsView;
  private bool _error = false;

  public int EnemyCount { get; set; }
  public int Score { get; set; }
  public EventHolder EventHolder
  {
    get { return _eventHolder; }
  }
  public EnemySpawnController EnemySpawnController
  {
    get { return _enemySpawnController; }
  }
  #endregion

	private void Start () 
  {
    CreateMash();
    InitObjects();

    if(!_error)
    {
      Action<string> getConfigDataCallback = GetConfigData;
      _fileManager.LoadConfigs(getConfigDataCallback);
    }
  }

  private void InitObjects()
  {
    if(_devMode)
    {
      _fpsView = CreateSceneObject<FpsView>();
    }
    _inputController = CreateSceneObject<InputController>();
    _configHolder = CreateSceneObject<ConfigHolder>();
    _eventHolder = CreateSceneObject<EventHolder>();
    _fileManager = CreateSceneObject<FileManager>();

    if(_playerPawn != null)
    {
      _playerPawnController = Instantiate(_playerPawn).GetComponent<PlayerPawnController>();
    }
    else
    {
      Debug.LogError(string.Format("[{0}][InitObjects]_playerPawn is null!", GetType().Name));
      _error = true;
    }

    if(_enemySpawnPrefab != null)
    {
      _enemySpawnController = Instantiate(_enemySpawnPrefab).GetComponent<EnemySpawnController>();
    }
    else
    {
      Debug.LogError(string.Format("[{0}][InitObjects]_enemySpawnController is null!", GetType().Name));
      _error = true;
    }
  }

  private T CreateSceneObject<T>() where T : Component
  { 
    return new GameObject(typeof(T).Name, typeof(T)).GetComponent<T>();
  }

  private void GetConfigData(string data)
  {
    if(!string.IsNullOrEmpty(data))
    {
      _configHolder.InitConfig(JsonUtility.FromJson<GameConfig>(data));
      _playerPawnController.InitContoller(_configHolder.PlayerPawnMoveSpeed, _configHolder.PlayerPawnMoveSpeed);
      _enemySpawnController.Spawn(true, _configHolder.EnemySpawnTime);
      _inputController.CheckInput(true, _configHolder.PlayerInputMoveTime);
    }
  }

  private GameObject CreateMash()
  {
    GameObject returnValue = new GameObject();
    returnValue.name = "Floor";
    returnValue.transform.parent = new GameObject("LevelObjects").transform;
    returnValue.transform.localScale = new Vector3(1, 1, 1);
    returnValue.transform.localPosition = new Vector3(-FLOOR_WIDTH / 2, 0, -FLOOR_HEIGHT / 2);
    MeshFilter meshFilter = returnValue.AddComponent<MeshFilter>();
    Mesh mesh = new Mesh();
    meshFilter.mesh = mesh;

    Vector3[] vertices = new Vector3[4];
    vertices[0] = new Vector3(0, 0, 0);
    vertices[1] = new Vector3(FLOOR_WIDTH, 0, 0);
    vertices[2] = new Vector3(0, 0, FLOOR_HEIGHT);
    vertices[3] = new Vector3(FLOOR_WIDTH, 0, FLOOR_HEIGHT);

    int[] tri = new int[6];
    tri[0] = 0;
    tri[1] = 2;
    tri[2] = 1;
    tri[3] = 2;
    tri[4] = 3;
    tri[5] = 1;

    Vector3[] normals = new Vector3[4];
    normals[0] = -Vector3.forward;
    normals[1] = -Vector3.forward;
    normals[2] = -Vector3.forward;
    normals[3] = -Vector3.forward;

    Vector2[] uv = new Vector2[4];
    uv[0] = new Vector2(0, 0);
    uv[1] = new Vector2(1, 0);
    uv[2] = new Vector2(0, 1);
    uv[3] = new Vector2(1, 1);

    mesh.vertices = vertices;
    mesh.triangles = tri;
    mesh.normals = normals;
    mesh.uv = uv;

    MeshRenderer meshRenderer = returnValue.AddComponent<MeshRenderer>();
    meshRenderer.material = _floorMaterial;
    return returnValue;
  }

  public void RotatePlayerPawn(Vector3 clickPoint)
  {
    _playerPawnController.ChangeRoration(clickPoint);
  }

  public void MovePlayerPawn(Vector3 clickPoint)
  {
    _playerPawnController.ChangePosition(clickPoint);
  }
}